<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeckEditorController extends AbstractController
{
    /**
     * @Route("/deck/editor", name="deck_editor")
     */
    public function index()
    {
        return $this->render('deck_editor/index.html.twig', [
            'controller_name' => 'DeckEditorController',
        ]);
    }
}
