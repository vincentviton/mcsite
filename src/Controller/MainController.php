<?php

namespace App\Controller;

use App\Entity\Card;
use App\Form\CardType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     */
    public function index()
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home(){
        return $this->render("home.html.twig");
    }



/**
 * @Route("/cardslist",name="cardslist")
 */
    public function showAllCards(){

        $repo = $this->getDoctrine()->getRepository(Card::class);
        $allCards = $repo->findAll();

        return $this->render("front/cardsList.html.twig",[
            'allCards'=>$allCards
        ]);
    }

    /**
     * @Route("/admin/create_card", name="create.card")
     * @Route("/admin/card/{id]/edit", name="edit.card")
     */
    public function cardForm(Card $card = null, Request $request, ObjectManager $manager){

        if (!$card) $card = new Card();

        $form = $this->createForm(CardType::class, $card);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($card);
            $manager->flush();
        }

        return $this->render("admin/createCard.html.twig",[
            'cardForm'=>$form->createView(),
            'editMode'=>$card->getId() !== null
        ]);
    }

    /**
     * @Route("/campain", name="campain")
     */
    public function campainMenu(){
        return $this->render("front/campain.html.twig");
    }

     /**
     * @Route("/mission/{id}", name="mission")
     */
    public function mission($id){
        return $this->render("front/mission.html.twig",[
            'missionID'=>$id
        ]);
    }

     /**
     * @Route("/complete_mission/{id}", name="complete.mission")
     */
    public function completeMission($id, ObjectManager $manager){
        $user = $this->getUser();
        if ($user->getLevel() <= $id){
            $user->setLevel($id+1);
            $manager->persist($user);
            $manager->flush();
        } 

        return $this->redirectToRoute('campain');
    }

    /**
     * @Route("/about", name="about")
     */
    public function about(){
        return $this->render("main/about.html.twig");
    }
}
