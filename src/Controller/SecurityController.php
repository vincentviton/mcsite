<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/security", name="security")
     */
    public function index()
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @Route("/register",name="security.register")
     */
    public function registration(Request $request ,ObjectManager $manager, UserPasswordEncoderInterface $encoder ){

        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $user->setVictories(0);
            $user->setDefeats(0);
            $user->setDraws(0);
            $user->setLevel(0);
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security.login');
        }

        return $this->render("security/register.html.twig",[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/login", name="security.login")
     */
    public function login(){
        return $this->render('security/login.html.twig');
    }
/**
 * @route("/logout", name="security.logout")
 */
    public function logout(){}
}
